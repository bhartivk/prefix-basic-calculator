#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include"bc.h"
void initValue(Value *ptr){
	(*ptr) = (Value ) malloc(sizeof(def));
	(*ptr)->sign = '+';
	(*ptr)->pos = 0;
	(*ptr)->tail = (*ptr)->head = NULL;
}
int NodesInValue(Value number) {
	int count = 0;
	list *temp = number->head;
	while(temp != NULL) {
		count += 1;
		temp = temp->next;
	}
	return (count);
}
int isDigit(char ch){
	return(ch >= '0' && ch <= '9');
}
void storenum(Value *a1, char *str) {
	initValue(a1);
	//printf("In storenum passed string: %s\n", str);
	if(str[0] == '-' || str[0] == '+') {
                (*a1)->sign = str[0];
		str++;
        }
	//printf("In storenum() str After assigning sign:%s\n", str);
	char *savestr1, *tok;
	int len, num = 0, length1, i, initial = 0;
	list *temp;
	tok = strtok_r(str, ".", &savestr1);
	len = strlen(tok);
	//printf("In storenum() tok:%s", tok);
	(*a1)->pos = len;
	for(i = 0; i < len ; ){
		if(i != (len - 1) && initial == 0 && tok[i] == '0'){
			i++;
			(*a1)->pos -= 1;
			continue;
		}
		if(isDigit(tok[i])){
	 		num = tok[i] - '0';
	 		initial = 1;
	 		i++;
	 	}
	 	else{
	 		(*a1) = NULL;
			return;
	 	}
	 	temp = (list *)malloc(1 * sizeof(list));
	 	temp->num = num;
	 	temp->next = NULL;
	 	if((*a1)->head == NULL){
			(*a1)->head = (*a1)->tail = temp;
			temp->prev = NULL;
		}
		else{
			temp->num = num;
			temp->prev = (*a1)->tail;
			(*a1)->tail->next = temp;
			(*a1)->tail = temp;
		}
	}
	length1 = strlen(savestr1);
	for(i = 0; i < length1;){
		if(isDigit(savestr1[i])){
			num = savestr1[i] - '0';
				i++;
		}
		else{
			(*a1) = NULL;
			return;
		}	
		temp = (list *)malloc(1 * sizeof(list));
	 	temp->num = num;
	 	temp->next = NULL;
		if((*a1)->head == NULL){
			(*a1)->head = (*a1)->tail = temp;
			temp->prev = NULL;
		}
		else{
			temp->num = num;
			temp->prev = (*a1)->tail;
			(*a1)->tail->next = temp;
			(*a1)->tail = temp;
		}
	}
	simpValue(a1);
	
}
Value add(Value num1, Value num2) {
	int i, absdiff, num = 0, carry = 0;
	Value num3;
	list *temp, *temptail1, *temptail2;
	initValue(&num3);
	int lenFracNum1 = NodesInValue(num1) - num1->pos;
	int lenFracNum2 = NodesInValue(num2) - num2->pos;
	absdiff = abs(lenFracNum1 - lenFracNum2);
	if(num1->pos > num2->pos)
		num3->pos = num1->pos;
	else
		num3->pos = num2->pos;
	if(lenFracNum1 > lenFracNum2){
		temptail1 = num1->tail;
		temptail2 = num2->tail;
	}
	else {
		temptail1 = num2->tail;
		temptail2 = num1->tail;
	}
	for(i = 0; i < absdiff; i++) {
		temp = (list *)malloc(sizeof(list));
		temp->num = temptail1->num;
		//printf("diff:%d\n", temp->num);
		temp->prev = NULL;
		if(num3->head == NULL) {
			temp->prev = temp->next = NULL;
			num3->head = num3->tail = temp;
		}
		else{
			temp->next = num3->head;
			num3->head->prev = temp;
			num3->head = temp;
		}
		temptail1 = temptail1->prev;
	}
	while(temptail1 != NULL || temptail2 != NULL){
		if(temptail1 != NULL && temptail2 != NULL){
			num = (temptail1->num) + (temptail2->num) + carry;
		//	printf("%d %d\n", num, temptail2->num);
			carry = num / 10;
			num = num % 10;
			temptail1 = temptail1->prev;
			temptail2 = temptail2->prev;
		}
		else if(temptail1 != NULL){
			num = (temptail1->num) + carry;
			carry = num / 10;
			num = num % 10;
			temptail1 = temptail1->prev;
		}
		else{
			num = (temptail2->num) + carry;
			carry = num /10;
			num = num % 10;
			temptail2 = temptail2->prev;
		}
		temp = (list *)malloc(sizeof(list));
		temp->num = num;
		temp->prev = NULL;
		if(num3->head == NULL) {
			temp->prev = temp->next = NULL;
			num3->head = num3->tail = temp;
		}
		else{
			temp->next = num3->head;
			num3->head->prev = temp;
			num3->head = temp;
		}
		
	}
	if(carry){
		temp = (list *)malloc(1 * sizeof(list));
		temp->num = carry;
		temp->prev = NULL;
		temp->next = num3->head;
		num3->head->prev = temp;
		num3->head = temp;
		num3->pos += 1;
	}
	return num3;
	
}
int abscmp(Value num1, Value num2) {	
	if(num1->pos > num2->pos) {
		return 1;
	}
	if(num1->pos < num2->pos) {
		return -1;
	}
	list *temp1, *temp2;
	temp1 = num1->head;
	temp2 = num2->head;
	while(temp1 != NULL && temp2 != NULL) {
		if(temp1->num > temp2->num) {
			return 1;
		}
		if(temp1->num < temp2->num) {
			return -1;
		}
		temp1 = temp1->next;
		temp2 = temp2->next;
	}
	while(temp1 != NULL){
		if(temp1->num > 0) {
			return 1;
		}
		temp1 = temp1->next;
	}
	while(temp2 != NULL){
		if(temp2->num > 0) {
			return -1;
		}
		temp2 = temp2->next;
	}
	return 0;
}
void simpValue(Value *num){
	list *temp = (*num)->head;
	int i = 0, flag = (*num)->pos;	
	while(temp->num == 0) {
		if(i < flag - 1){
			(*num)->head = (*num)->head->next;
			(*num)->head->prev = NULL;
			free(temp);
			(*num)->pos -= 1;
			temp = (*num)->head;
			i++;
		}
		else
			break;
	}
	temp = (*num)->tail;
	i = 0;
	flag = NodesInValue(*num) - (*num)->pos;
	while(temp->num == 0 && i < flag) {
		(*num)->tail = (*num)->tail->prev;
		(*num)->tail->next = NULL;
		free(temp);
		temp = (*num)->tail;
		i++;
	}
}
Value subtract(Value num1, Value num2) {
	int i, absdiff, num = 0, carry = 0;
	Value num3;
	list *temp, *temptail1, *temptail2;
	initValue(&num3);
	int lenFracNum1 = NodesInValue(num1) - num1->pos;
	int lenFracNum2 = NodesInValue(num2) - num2->pos;
	absdiff = abs(lenFracNum1 - lenFracNum2);
	num3->pos = num1->pos;
	//else
	//	num3->pos = num2->pos;
	temptail1 = num1->tail;
	temptail2 = num2->tail;
	if(lenFracNum1 > lenFracNum2){
		for(i = 0; i < absdiff; i++) {
			temp = (list *)malloc(sizeof(list));
			temp->num = temptail1->num;
			//printf("diff:%d\n", temp->num);
			temp->prev = NULL;
			if(num3->head == NULL) {
				temp->prev = temp->next = NULL;
				num3->head = num3->tail = temp;
			}
			else{
				temp->next = num3->head;
				num3->head->prev = temp;
				num3->head = temp;
			}
			temptail1 = temptail1->prev;
		}
	}
	else {
		for(i = 0; i < absdiff; i++) {
			temp = (list *)malloc(sizeof(list));
			if((temptail2->num + carry) > 0) {
				temp->num = 10 - (temptail2->num + carry);
				carry = 1;
			}
			else{
				temp->num = 0;
				carry = 0;
			}
			//printf("diff:%d\n", temp->num);
			temp->prev = NULL;
			if(num3->head == NULL) {
				temp->prev = temp->next = NULL;
				num3->head = num3->tail = temp;
			}
			else{
				temp->next = num3->head;
				num3->head->prev = temp;
				num3->head = temp;
			}
			temptail2 = temptail2->prev;
		}
	}
	while(temptail1 != NULL || temptail2 != NULL){
		if(temptail1 != NULL && temptail2 != NULL){
			if((temptail2->num + carry) > (temptail1->num)){
				num = (temptail1->num + 10) - (temptail2->num + carry);
				carry = 1;
			}
			else{
				num = (temptail1->num) - (temptail2->num + carry);
				carry = 0;
			}
		//	printf("%d %d\n", num, temptail2->num);
			temptail1 = temptail1->prev;
			temptail2 = temptail2->prev;
		}
		else if(temptail1 != NULL){
			if(carry > (temptail1->num)){
				num = (temptail1->num + 10) - carry;
				carry = 1;
			}
			else {
				num = temptail1->num - carry;
				carry = 0;
			}
			temptail1 = temptail1->prev;
		}
		temp = (list *)malloc(sizeof(list));
		temp->num = num;
		temp->prev = NULL;
		if(num3->head == NULL) {
			temp->prev = temp->next = NULL;
			num3->head = num3->tail = temp;
		}
		else{
			temp->next = num3->head;
			num3->head->prev = temp;
			num3->head = temp;
		}	
	}
//**	print(num3);
	simpValue(&num3);
	return num3;
}
Value addition(Value num1, Value num2){
	Value num3;
	char str[] = {'0', '\0'};
	int cmp;
	if(num1->sign == num2->sign){
		num3 = add(num1, num2);
		num3->sign = num1->sign;
		return num3;
	}
	else{
		cmp = abscmp(num1, num2);
		//**printf("From addition cmp:%d\n", cmp);
		//**print(num1);
		//**print(num2);
		if(cmp == 0){
			storenum(&num3, str);
			print(num3);
			return num3;
		}
		if(cmp == 1){
			num3 = subtract(num1, num2);
			num3->sign = num1->sign;
			return num3;
		}
		else{
			num3 = subtract(num2, num1);
			num3->sign = num2->sign;
			return num3;
		}
	}
}
Value subtraction(Value num1, Value num2){
	Value num3;
	char str[] = {'0', '\0'};
	int cmp;
	if(num1->sign != num2->sign){
		num3 = add(num1, num2);
		num3->sign = num1->sign;
		return num3;
	}
	else{
		cmp = abscmp(num1, num2);
		//**printf("From addition cmp:%d\n", cmp);
		//**print(num1);
		//**print(num2);
		if(cmp == 0){
			storenum(&num3, str);
			print(num3);
			return num3;
		}
		if(cmp == 1){
			num3 = subtract(num1, num2);
			num3->sign = num1->sign;
			return num3;
		}
		else{
			num3 = subtract(num2, num1);
			if(num2->sign == '-')
				num3->sign = '+';
			else
				num3->sign = '-';
			return num3;
		}
	}
}
Value multipliaction(Value num1, Value num2) {
	list *temp1, *temp2 = num2->tail, *temp;
	Value num3, result;
	int lenFracNum1 = NodesInValue(num1) - num1->pos;
	int lenFracNum2 = NodesInValue(num2) - num2->pos;
	int i, k = 0, carry;
	while(temp2 != NULL) {
		temp1 = num1->tail;
		carry = 0;
		initValue(&num3);
		for(i = 0; i < k; i++){
			temp = (list *)malloc(sizeof(list));
			temp->num = 0;
			temp->prev = NULL;
			if(num3->head == NULL) {
				temp->prev = temp->next = NULL;
				num3->head = num3->tail = temp;
			}
			else{
				temp->next = num3->head;
				num3->head->prev = temp;
				num3->head = temp;
			}
			(num3->pos)++; 
		}
		while(temp1 != NULL) {
			temp = (list *)malloc(sizeof(list));
			temp->num = ((temp1->num * temp2->num) + carry) % 10;
			carry = ((temp1->num * temp2->num) + carry) / 10;
			temp->prev = NULL;
			if(num3->head == NULL) {
				temp->prev = temp->next = NULL;
				num3->head = num3->tail = temp;
			}
			else{
				temp->next = num3->head;
				num3->head->prev = temp;
				num3->head = temp;
			}
			(num3->pos)++;
			temp1 = temp1->prev;
		}
		if(carry){
			temp = (list *)malloc(1 * sizeof(list));
			temp->num = carry;
			temp->prev = NULL;
			temp->next = num3->head;
			num3->head->prev = temp;
			num3->head = temp;
			num3->pos++;
		}
		if(k != 0) {
			result = add(result, num3);
		}
		else {
			result = num3;
		}
		temp2 = temp2->prev;
		k++;
			
	}
	result->pos = result->pos - (lenFracNum1 + lenFracNum2);
	if(num1->sign != num2->sign)
		result->sign = '-';
	simpValue(&result);
	return result;
}
/*Value division(Value num1,Value num2){
	char str[] = "0";
	storenum(&result,str);
	Value result = NULL, tmpNum1;
	initNumber(&tmpNum1);
	initNumber(&result);
	list *tmp = num1->head, *temp;
	
	return NULL;
}*/
void destroy(Value *num){
	list *temp = (*num)->head;
	while(temp){
		(*num)->head = (*num)->head->next;
		if((*num)->head){
			(*num)->tail = (*num)->head->prev = NULL;
		}
		free(temp);
		temp = (*num)->head;
	}
	free(*num);
}
void print(Value a){
	list *temp = a->head;
	int i;
	if(a->sign != '+')
		printf("%c", a->sign);
	for(i = 0; temp != NULL; i++){
		if(i == a->pos){
			printf("%c", '.');
			printf("%d",temp->num);
		}
		else
			printf("%d", temp->num);
		temp = temp->next;
	}
	printf("\n");
}
