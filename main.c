#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"stack.h"
#include<string.h>
int optrchar(char ch) {
	if(ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '^' || ch == '%')
		return 1;
	return 0;
}
Value eval(char ch, Value opnd1, Value opnd2) {
	switch(ch) {
	case '+':
		return addition(opnd1, opnd2);
	case '-':
		return subtraction(opnd1, opnd2);
	case '*':
		return multipliaction(opnd1, opnd2);
	default:
		printf("Invalid operation:");
		return NULL;
	}
}

int isSign(char str) {
	if(str == '+' || str == '-')
		return 1;
	else
		return 0;
}
Value prefix(char **str) {
	Value opnd[2], value, num1 = NULL;
	char ch, seq[35];
	int length, i = 0, j;
	char *saveptr, *ptr;
	opndstack opndstk, reverse;
	optrstack optrstk;
	optrinit(&optrstk);
	opndinit(&opndstk);
	opndinit(&reverse);
	while(**str != '\0') {
		if(isDigit(**str) || (isSign(**str) && isDigit(*(*str + 1)))){
			ptr = (char *)malloc((strlen(*str)+1) * sizeof(char));
			strcpy(ptr, *str);
			strtok_r(ptr, " ", &saveptr);
			length = strlen(ptr);
			(*str) = (*str) + length;
			storenum(&num1, ptr);
			if(num1 == NULL)
				return NULL;
			opndpush(&opndstk, num1);
			seq[i++] = 'n';
			free(ptr);
		} 
		else if(optrchar(**str)) {
			optrpush(&optrstk, **str);
			seq[i++] = 'c';
		}
		else if(!(**str == ' ' || **str == '\t')){
			return NULL;
		}
		(*str)++;
	}
	while(i > 0){
		switch(ch = seq[--i])
		{
			case 'n':
				opndpush(&reverse, opndpop(&opndstk));
			break;
			case 'c':
				for(j = 0; j <= 1; j++){
					if(!(opndisempty(&reverse))){
						opnd[j] = opndpop(&reverse);
					}
					else{
						return NULL;
					}
				}
				ch = optrpop(&optrstk);
				value = eval(ch, opnd[0], opnd[1]);
				if(value == NULL)
					return NULL;
				opndpush(&reverse, value);
				destroy(opnd+0);
				destroy(opnd+1);
			break;
		}
	}
	value = opndpop(&reverse);
	if(opndisempty(&reverse))
		return value;
	return NULL;
}
int main() {
	int i;
	char ch;
	char *str, *ptr;
	Value num;
	printf("Enter the input:\n");
	while(1){
		i = 0;
		for(i = 0; (ch = getchar()) != '\n';){
			if(i == 0){
				str = (char *)malloc(sizeof(char));
			}
			str[i++] = ch;
			str = (char *)realloc(str, (i+2) * (sizeof(char)));	
		}
		str[i] = '\0';
		ptr = str;
		if(strcmp(str, "q") == 0){
			exit(EXIT_SUCCESS);
		}
		else{
			str = ptr;
			num = prefix(&str);
			if(num == NULL){
				printf(" input is wrong\n");
				continue;
			}
			print(num);
		}
		free(ptr);
	}
	return 0;
}
