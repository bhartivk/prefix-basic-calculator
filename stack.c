#include"stack.h"
#include<stdlib.h>
void opndinit(opndstack *l) {
	*l = NULL;
}
void optrinit(optrstack *l) {
	*l = NULL;
}
void optrpush(optrstack *l, char ch) {
	node2 *tmp = (node2 *) malloc(sizeof(node2));
	tmp-> ch = ch;
	tmp->next = *l;
	*l = tmp;
}
char optrpop(optrstack*l) {
	char temp;
	node2 *tmp;
	temp = (*l)-> ch;
	tmp = *l;
	(*l)= (*l)->next;
	free(tmp);
	return temp;
}
int optrisempty(optrstack l) {
	return l == NULL;
}
int isfulloptr(optrstack *l) {
	return 0;
} 
int opndisempty(opndstack *l) {
	return *l == NULL;
}
int opndisfull(opndstack *l) {
	return 0;
} 
void opndpush(opndstack *l, Value num) {
	node1 *tmp = (node1 *) malloc(sizeof(node1));
	tmp->val = num;
	tmp->next = *l;
	*l = tmp;
}
Value opndpop(opndstack*l){
	Value temp;
	temp = (*l)->val;
	(*l)= (*l)->next;
	return temp;
}
