#include<stdio.h>
typedef struct list {
	int num;
	struct list *next, *prev;
} list;
typedef struct def{
	int pos;
	char sign;
	list *head;
	list *tail;
} def;
typedef def* Value;
void initValue(Value *ptr);
void storenum(Value *a1, char *str);
int NodesInValue(Value number);
void destroy(Value *num);
void simpValue(Value *num);
void print(Value a);
Value addition(Value num1, Value num2);
Value subtraction(Value num1, Value num2);
Value multipliaction(Value num1, Value num2);
Value division(Value num1, Value num2);
int isDigit(char ch);
