#include"bc.h"
typedef struct node1 {
	Value val;
	struct node1 *next;
}node1;
typedef node1 *opndstack;
typedef struct node2 {
	char ch;
	struct node2 *next;
}node2;
typedef node2 *optrstack;
void optrinit(optrstack *l) ;
void optrpush(optrstack *l, char ch);
char optrpop(optrstack *l);
int optrisempty(optrstack l);
int optrisfull(optrstack *l);
void opndinit(opndstack *l);
void opndpush(opndstack *l, Value num);
Value opndpop(opndstack*l);
int opndisempty(opndstack *l);
int opndisfull(opndstack *l);
